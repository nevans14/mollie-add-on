=== Mollie Add-on for Studiocart ===

Contributors: ncstudio
Requires at least: 5.0.1
Tested up to: 5.7
Stable tag: 2.1.17
Requires PHP: 7.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Add the Mollie payment gateway to Studiocart order forms

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Settings->Studiocart->Settings screen to configure the plugin

== Changelog ==

= 1.2.2 =
* Bug fixes

= 1.2.1 =
* Bug fixes

= 1.2.0 =
* Freemius integration for automatic updates

= 1.1.2 =
* Update: Mollie error handling

= 1.1.0 =
* Bug fixes
 
= 1.0.0 =
* First Release