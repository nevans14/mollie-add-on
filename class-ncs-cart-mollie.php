<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://ncstudio.co
 * @since      1.0.0
 *
 * @package    NCS_Cart
 * @subpackage NCS_Cart/public
 */
/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    NCS_Cart
 * @subpackage NCS_Cart/public
 * @author     N.Creative Studio <info@ncstudio.co>
 */
require_once( plugin_dir_path( __FILE__ ) . '/vendor/autoload.php');
use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\MollieApiClient;
use Mollie\Api\Types\PaymentMethod;
use Mollie\Api\Types\MandateMethod;
class NCS_Cart_Mollie extends NCS_Cart_Public {
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
    private $mollieApiKey, 
    		$mollieApiMethod,
    		$mollieApiCreditMethod, 
    		$mollieEnable,
    		$molliePendingPaymentMessage,
    		$mollieFailedPaymentMessage,
    		$mollieCanceledPaymentMessage,
    		$mollieExpirePaymentMessage;
    public function __construct() {
        $this->mollieEnable = get_option( '_sc_mollie_enable',0 );
        $mollie_api = get_option( '_sc_mollie_api','test' );
        $this->mollieApiKey = get_option('_sc_mollie_'.$mollie_api.'_api_key');
        $this->mollieApiCreditMethod = get_option('_sc_mollie_creditcard_method');
        $this->mollieApiMethod = get_option('_sc_mollie_ideal_method');
        $this->mollieApiMethod = get_option('_sc_mollie_ideal_method');
        add_action( 'init', [ $this, 'init' ], 10 );
        $this->setup_ajax();
        $this->setup_mollie();
        $this->set_locale();
        $this->molliePendingPaymentMessage = get_option( '_sc_mollie_pending_error_message','Thank you. Order get process after Payment confirmation.' );
		$this->mollieFailedPaymentMessage = get_option( '_sc_mollie_fail_error_message','There was a problem with this payment, please try another payment method.' );
		$this->mollieCanceledPaymentMessage = get_option( '_sc_mollie_cancel_error_message','The payment has been canceled.' );
		$this->mollieExpirePaymentMessage = get_option( '_sc_mollie_expire_error_message','This payment is past due.' );
    }
    public function init() {
        global $sc_currency;
        
        add_action( 'init', [ $this, 'sc_mollie_process_payment' ], 9999 ); 
        if($this->mollieApiKey && $this->mollie):
            //Public Hooks
            //add Payment method to payment page
            add_filter('sc_payment_methods', [ $this, 'sc_mollie_radio' ], 10, 2);
            if($this->mollieMethod['ideal'] == '1' && strtoupper($sc_currency)=='EUR') :
                //Add Payment Feilds
                add_action('sc_before_payment_info', [ $this, 'sc_mollie_issuer' ],10,1 );
            endif;
            //On submit payment form
            add_action( 'sc_checkout_form_scripts', [ $this, 'mollie_checkout_form_scripts'], 10 );
            // Exicute Upsell and downsell purchase
            add_action( 'wp_footer', array($this, 'sc_mollie_upsell_scripts'));
            //Add payment gateway to payment options
            add_filter( 'sc_product_payments_fields', [ $this, '_sc_mollie_payments_fields'], 10,2 );
            //Show After Payment error(In Case of redirect)
            add_filter( 'sc_checkout_page_error', [ $this, '_sc_mollie_payments_error'], 10,1 );
        endif;
        //Add new webhook for gateway
        add_action( 'sc_gateway_webhook', array($this, 'sc_mollie_webhook'));
        //Admin Hooks
        //Show txn link on order page
        add_filter('studiocart_order_details_link', [ $this, 'mollie_order_details_link' ], 10, 2);
        add_filter('studiocart_subscription_details_link', [ $this, 'mollie_subscription_details_link' ], 10, 2);
        //Add Payment gateway section in setting
        add_action( '_sc_payment_gateway_tab_section', array($this, '_sc_register_sections'),10,1);
        //Payment Gateway setting Fields
        add_filter( '_sc_payment_field_option_list', [ $this, '_sc_mollie_option'], 10,1 );
        //Product page setting for payment gateway section
        add_filter( 'sc_enabled_processors', [ $this, '_sc_mollie_enabled_processors'], 10,1 );
        add_action( 'sc_checkout_page_heading', array($this, 'sc_mollie_checkout_page'),10);
        add_filter( 'sc_payment_intent', array($this, 'sc_mollie_payment_intent'),10,2);//$order->stripe_charge_id ,$order );
        //add_filter( 'sc_order_method', array($this, 'sc_mollie_payment_method'),10,2);//$order->stripe_charge_id ,$order );
    }

    public function setup_mollie(){
        if($this->mollieApiKey):
            
            $this->mollie = new MollieApiClient();
            $this->mollie->setApiKey($this->mollieApiKey);
                $methods = $this->mollie->methods->allActive();
                if(!empty($methods)){
                    $this->active_methods = array();
                    foreach($methods as $method):
                        $svg = '';
                        if(isset($method->image) && isset($method->image->svg)){
                            $svg = "<img src='{$method->image->svg}'>";
                        }
                        $this->active_methods[$method->id] = array('title'=>str_replace('Button','',$method->description),'svg'=>$svg);
                        
                        add_action( 'sc_order_refund_mollie_'.$method->id,array($this, 'sc_mollie_refund'),10,1);
                        add_filter( 'sc_subscription_cancel_mollie_'.$method->id,array($this, 'sc_mollie_subscription_cancel'),10,2);
                    endforeach;
                    foreach($this->active_methods as $active_key => $active_method):
                        $this->mollieMethod[$active_key] = get_option("_sc_mollie_{$active_key}_method");
                        
                    endforeach;
                } else {
                    $this->mollieMethod = array();
                }
        endif;
    }

    /**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the NCS_Cart_Mollie_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new NCS_Cart_Mollie_i18n();

		add_action( 'init', [$plugin_i18n, 'load_plugin_textdomain'] );

	}
    
    public function setup_ajax(){
        add_action( 'wp_ajax_sc_mollie_request', [ $this, 'sc_mollie_request' ] );
        add_action( 'wp_ajax_nopriv_sc_mollie_request', [ $this, 'sc_mollie_request' ] );
        add_action( 'wp_ajax_sc_mollie_method_change_request', [ $this, 'sc_mollie_method_change_request' ] );
        add_action( 'wp_ajax_nopriv_sc_mollie_method_change_request', [ $this, 'sc_mollie_method_change_request' ] );
        add_action( 'wp_ajax_mollie_process_upsell', [ $this, 'mollie_process_upsell__premium_only' ] );
        add_action( 'wp_ajax_nopriv_mollie_process_upsell', [ $this, 'mollie_process_upsell__premium_only' ] );
        add_filter( 'studiocart_transaction_id', [ $this, 'mollie_transaction_id' ], 10, 3 );
        add_filter( 'studiocart_subscription_transaction_id', [ $this, 'mollie_sub_transaction_id' ], 10, 3 );
    }

    public function sc_mollie_payment_intent($payment_intent, $order){
        if(!isset($order->pay_method)):
            return $payment_intent;
        endif;
        if(empty($this->mollieMethod)){
            return $payment_intent;
        }
        if (in_array(str_replace('mollie_','',$order->pay_method),array_keys($this->mollieMethod))):
            if($payment_intent = get_post_meta($order->ID, 'mollie_payment_id', true)):
                return $payment_intent;
            endif;
        endif;
        return $payment_intent;
    }

    public function mollie_order_details_link($link, $order) {
        
        if(!isset($order->pay_method)):
            return $link;
        endif;
        if(empty($this->mollieMethod)){
            return $link;
        }
        if (in_array(str_replace('mollie_','',$order->pay_method),array_keys($this->mollieMethod))){
            if (!$id = get_post_meta($order->ID, 'mollie_payment_id', true)) {
                return esc_html( 'Awaiting confirmation from Mollie.', 'studiocartmollie' );
            } else {
                switch(strtolower($order->status)){
                    case('uncollectible'):
                        $link = esc_html( 'Unable to confirmation from Mollie.', 'studiocartmollie' );
                    break;
                    case('canceled'):
                        $link = esc_html( 'Payment Canceled.', 'studiocartmollie' );
                    break;
                    case('past_due'):
                        $link = esc_html( 'Payment link expire from Mollie.', 'studiocartmollie' );
                        break;   
                    case('pending-payment'):
                    case('pending'):
                        $link = esc_html( 'Awaiting confirmation from Mollie.', 'studiocartmollie' );
                    break;   
                    default:
                        $link = esc_html( 'Order Completed.', 'studiocartmollie' );
                    break;
                }
                return esc_html( $link.' Mollie Payment ID: ', 'studiocartmollie' ).'<a id="'.$order->pay_method.'-id" href="https://www.mollie.com/dashboard/payments/'. $id .'" target="_blank">'.$id.'</a>';
            }
        }
        return $link;
    }
    public function mollie_subscription_details_link($link, $subscription) {
        if(!isset($subscription->pay_method)):
            return $link;
        endif;
        if(empty($this->mollieMethod)){
            return $link;
        }
        if (in_array(str_replace('mollie_','',$subscription->pay_method),array_keys($this->mollieMethod))){  
             $id = get_post_meta($subscription->ID, 'mollie_subscription_id', true);         
                $id = $subscription->mollie_subscription_id;
                switch($subscription->pay_method){
                    case('uncollectible'):
                        $link = esc_html( 'Unable to confirmation from Mollie.', 'studiocartmollie' );
                        break;
                    case('canceled'):
                        $link = esc_html( 'Payment Canceled.', 'studiocartmollie' );
                        break;
                    case('past_due'):
                        $link = esc_html( 'Payment link expire from Mollie.', 'studiocartmollie' );
                        break;   
                    case('pending-payment'):
                    case('pending'):
                        $link = esc_html( 'Awaiting confirmation from Mollie.', 'studiocartmollie' );
                        break;   
                    default:
                        $link = esc_html( 'Order Completed.', 'studiocartmollie' );
                    break;
                }
                return esc_html( $link.' Mollie Payment ID: ', 'studiocartmollie' ).'<a id="mollie-id" href="https://www.mollie.com/dashboard/payments/'. $id .'" target="_blank">'.$id.'<input type="hidden" name="sc_payment_intent" id="stripe_sc_payment_intent" value="'.$id.'"></a>';
        }
        return $link;
    }
    public function enabled_processors_list($processors) {
        if(empty($this->mollieMethod)){
            return $processors;
        }
        foreach($this->mollieMethod as $mollieMethod => $mollieMethodStatus):
            if($mollieMethodStatus):
                $processors[] = esc_html($this->active_methods[$mollieMethod]['title'],'studiocartmollie'); 
            endif;
        endforeach;
        return $processors;
    }
    public function _sc_mollie_option($options){
        $options['mollie']= array(
            'mollie-gateway' => array(
                'type'          => 'checkbox',
                'label'         => esc_html__( 'Enable', 'studiocartmollie' ),
                'settings'      => array(
                    'id'            => '_sc_mollie_enable',
                    'value'         => '',
                    'description'   => '',
                ),
                'tab'=>'payment',
            ),
            'mollie-api' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'API', 'studiocartmollie' ),
                'settings'      => array(
                    'id'            => '_sc_mollie_api',
                    'value' 		=> 'test',
                    'selections' 	=> array(
                        'test' => __('Test', 'studiocartmollie'),
                        'live' => __('Live', 'studiocartmollie')
                    ),
                ),
                'tab'=>'payment',
            ),
            'mollie-test-api-key' => array(
                'type'          => 'text',
                'label'         => esc_html__( 'Test Api Key', 'studiocartmollie' ),
                'settings'      => array(
                    'id'            => '_sc_mollie_test_api_key',
                    'value' 		=> '',
                    'description' 	=> '',
                ),
                'tab'=>'payment',
            ),
            'mollie-live-api-key' => array(
                'type'          => 'text',
                'label'         => esc_html__( 'Live Api Key', 'studiocartmollie' ),
                'settings'      => array(
                    'id'            => '_sc_mollie_live_api_key',
                    'value' 		=> '',
                    'description' 	=> '',
                ),
                'tab'=>'payment',
            )
        );
        if(!empty($this->active_methods)):
            foreach($this->active_methods as $method_id => $active_method):
                $options['mollie']['mollie-'.$method_id.'-method'] = array(
                    'type'          => 'checkbox',
                    'label'         => esc_html__( $active_method['title'], 'studiocartmollie' ),
                    'settings'      => array(
                        'id'            => "_sc_mollie_{$method_id}_method",
                        'value' 		=> $method_id
                    ),
                    'tab'=>'payment',
                );
            endforeach;
        else:
        endif;
        $options['mollie']['mollie-pending-payment']=array(
            'type'          => 'text',
            'label'         => esc_html__( 'Pending Payment Error Message', 'studiocartmollie' ),
            'settings'      => array(
                'id'            => '_sc_mollie_pending_error_message',
                'value' 		=> '',
                'description' 	=> '',
            ),
            'tab'=>'payment',
        ); 
        $options['mollie']['mollie-failed-payment']=array(
            'type'          => 'text',
            'label'         => esc_html__( 'Failed Payment Error Message', 'studiocartmollie' ),
            'settings'      => array(
                'id'            => '_sc_mollie_fail_error_message',
                'value' 		=> '',
                'description' 	=> '',
            ),
            'tab'=>'payment',
        ); 
        $options['mollie']['mollie-cancel-payment']=array(
            'type'          => 'text',
            'label'         => esc_html__( 'Cancel Payment Error Message', 'studiocartmollie' ),
            'settings'      => array(
                'id'            => '_sc_mollie_cancel_error_message',
                'value' 		=> '',
                'description' 	=> '',
            ),
            'tab'=>'payment',
        ); 
        $options['mollie']['mollie-expire-payment']=array(
            'type'          => 'text',
            'label'         => esc_html__( 'Expire Payment Error Message', 'studiocartmollie' ),
            'settings'      => array(
                'id'            => '_sc_mollie_expire_error_message',
                'value' 		=> '',
                'description' 	=> '',
            ),
            'tab'=>'payment',
        );
        return $options;
    }
    public function _sc_register_sections($intigrations){    
        $intigrations['mollie'] =  'Mollie';
        return $intigrations;
    }
    public function _sc_mollie_enabled_processors($processors)
    {
        if(empty($this->mollieMethod)){
            return $processors;
        }
        foreach($this->mollieMethod as $mollieMethod => $mollieMethodStatus):
            if($mollieMethodStatus):
                $processors[] = esc_html($this->active_methods[$mollieMethod]['title'],'studiocartmollie'); 
            endif;
        endforeach;
        return  $processors;
    }
    public function sc_mollie_radio($payment_methods, $post_id) {
        global $sc_currency;
        if(empty($this->mollieMethod)){
            return $payment_methods;
        }
        foreach($this->mollieMethod as $mollieMethod => $mollieMethodStatus):
            if($mollieMethodStatus):
                if($mollieMethod == 'ideal' && strtoupper($sc_currency)!='EUR'){
                    continue;
                }
                $payment_methods['mollie_'.$mollieMethod] = [
                    'value' => esc_html__('mollie_'.$mollieMethod, 'studiocartmollie'),
                    'label' => esc_html__($this->active_methods[$mollieMethod]['title'], 'studiocartmollie'),
                ];
            endif;
        endforeach;
        return $payment_methods;
    }
    public function _sc_mollie_payments_fields( $payment_methods, $post_id)
    {  
        global $sc_currency;
        if(empty($this->mollieMethod)){
            return $payment_methods;
        }
        foreach($this->mollieMethod as $mollieMethod => $mollieMethodStatus):
            if($mollieMethodStatus):
                if($mollieMethod == 'ideal' && strtoupper($sc_currency)!='EUR'){
                    continue;
                }
                $payment_methods[] = array(
                    'class'		=> 'widefat',
                    'description'	=> '',
                    'id'			=> '_sc_disable_'.$mollieMethod,
                    'label'		=> __($this->active_methods[$mollieMethod]['title'],'studiocartmollie'),
                    'placeholder'	=> '',
                    'type'		=> 'checkbox',
                    'value'		=> '',
                    'class_size'		=> ''
                );
            endif;
        endforeach;
        return $payment_methods;
    }
    public function sc_mollie_issuer($post_id){
        if(!empty($this->mollieApiKey)){
            if($this->mollie){
                $method = $this->mollie->methods->get(PaymentMethod::IDEAL, ["include" => "issuers"]);
            }
            ?>
            <div class="row sc-mollie">
                <div class="form-group col-sm-12 sc-mollie-single">
                    <label for="card-element">
                       <?php esc_html_e("Bank:", "studiocartmollie"); ?> <span class="req">*</span>
                    </label>
                    <input type="hidden" name="_sc_current_post" value="<?php the_ID(); ?>">
                    <select name="issuer" class="form-control required">
                        <option value="">- <?php esc_html_e("Select your bank", "studiocartmollie"); ?> -</option>
                        <?php
                        if(!empty($method)):
                            foreach ($method->issuers() as $issuer) :
                                echo '<option value=' . htmlspecialchars($issuer->id) . '>' . htmlspecialchars($issuer->name) . '</option>';
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>
            <script type="text/javascript">
                function toggleMollieMethods(){
                    var method = jQuery('[name="pay-method"]:checked').val();
                    var is_sub = jQuery('[name="sc_product_option"]:checked').data('installments');
                    if (method != 'mollie_ideal'){
                        jQuery('.sc-mollie').fadeOut();
                    } else {
                        jQuery('.sc-mollie').fadeIn();
                    }
                }
                jQuery('document').ready(function($){
                    toggleMollieMethods();
                    $('[name="pay-method"]').change(function(){
                        toggleMollieMethods();
                    });
                    $('[name="sc_product_option"]').change(function(){
                        toggleMollieMethods();
                    });
                });
            </script>
            <?php
        }
    }
    public function sc_mollie_upsell_scripts(){
        if (in_array(str_replace('mollie_','',$_POST['sc_order']['pay_method']),array_keys($this->mollieMethod))){
            global $scp;
            ?>
            <script type="text/javascript">
            jQuery('document').ready(function($){
                let mollie_method = <?= json_encode(array_keys($this->mollieMethod)); ?>;
                if (mollie_method.includes(studiocart.pay_method.replace("mollie_", ""))) {
                    $(document).on('studiocart/upsell/ready/accept_link','.sc-accept-upsell-link', function() {
                        $('a[href*="sc-upsell-offer=yes"],.sc-accept-upsell-link').attr('href','#');
                    });
                    $(document).on('studiocart/upsell/ready/decline_link','.sc-decline-upsell-link', function() {
                        $('a[href*="sc-upsell-offer=no"],.sc-decline-upsell-link').attr('href',studiocart.upsell_decline+'&sc-pp=1');
                    });
                }
                let event_action = "";
                $.each( mollie_method, function( key, value ) {
                    event_action +=" studiocart/upsell/accept/mollie_"+value;
                });
                $(document).on(event_action, '.sc-accept-upsell-link', function(event) {
                    us_data = {
                        'action': 'mollie_process_upsell',
                        'sc-order': studiocart.sc_order,
                        'sc-nonce': studiocart.sc_nonce,
                        'cancel_url': window.location.href,
                        'return_url': studiocart.upsell_accept,
                    };
                    if( 'undefined' !== typeof studiocart.is_downsell && '1' == studiocart.is_downsell ) {
                        us_data['downsell'] = 1;
                    }
                    $.post(studiocart.ajax, us_data, function(res) {
                        var response = JSON.parse(res);
                        if ('undefined' !== typeof response.error) {
                          window.location.href = studiocart.upsell_decline+'&sc-pp=1';
                        } else {
                          window.location.href = response.url;
                        }
                    });
                });
            });
            </script>
            <?php
        }
    }
    public function mollie_process_upsell__premium_only() {
        global $sc_stripe, $sc_currency;
        $is_downsell = isset($_POST['downsell']);
        if ( !isset($_POST['sc-order']) || !isset( $_POST['sc-nonce'] ) ){
            echo json_encode(array(
                'message' => __("There was a problem processing this order, please contact us for assistance.", "studiocartmollie"), 
            ));
        }
        if ($is_downsell) {
            if ( !wp_verify_nonce( $_POST['sc-nonce'], 'studiocart_downsell-'.intval($_POST['sc-order']) ) ){
                echo json_encode(array(
                    'message' => __("There was a problem processing this order, please contact us for assistance.", "studiocartmollie"), 
                ));
            }
        } else {
            if ( !wp_verify_nonce( $_POST['sc-nonce'], 'studiocart_upsell-'.intval($_POST['sc-order']) ) ){
                echo json_encode(array(
                    'message' => __("There was a problem processing this order, please contact us for assistance.", "studiocartmollie"), 
                ));
            }
        }
        $order_id = intval($_POST['sc-order']);
        $order_info = (array) sc_setup_order($order_id);
        $product_info = sc_setup_product($order_info['product_id']);        
        $amount = number_format( (float)$product_info->us_price, 2, '.', '');
        $url_var = 'sc-oto';
        $order_info['item_name'] = null;
        $order_info['name'] = null;
        $order_info['plan_id'] = null;
        $order_info['coupon'] = false;
        $order_info['option_type'] = 'one-time';
        unset($order_info['bump_id'],$order_info['bump_amt']);
        if ($is_downsell) {
            if ($product_info->downsell) {
                $order_info['amount'] = number_format( (float)$product_info->ds_price, 2, '.', '');
                $order_info['product_id'] = $product_info->ds_product;
                $order_info['order_type'] = 'downsell';
                $url_var = 'sc-oto-2';
            }
        } elseif ($product_info->upsell) {
            $order_info['amount'] = number_format( (float)$product_info->us_price, 2, '.', '');
            $order_info['product_id'] = $product_info->us_product;
            $order_info['order_type'] = 'upsell';
        }
        // save order to db        
        $post_id = $this->do_mollie_order_save($order_info);
        if ($post_id){
            $return_url = add_query_arg(array(
                $url_var => $post_id,
            ), $_POST['return_url']);
            $data = [
                'cancel_url'    => esc_url_raw($_POST['cancel_url']),
                'return_url'    => esc_url_raw($return_url),
                'email'         => sanitize_email( $order_info['email'] ),
                'first_name'    => sanitize_text_field( $order_info['firstname'] ),
                'last_name'     => sanitize_text_field( $order_info['lastname'] ),
                'sc_product_id' => $order_info['product_id'],
                'amount'        => $order_info['amount'],
                'order_id'      => $post_id,
                'item_name'     => get_the_title($order_info['product_id']),
                'pay_method'    => $order_info['pay_method'],
                'mollie_issuer' => $order_info['mollie_issuer'],
            ];
            $mollieUrl = $this->sc_build_mollie_url($data);
            $res = ['url'=>$mollieUrl, 'data'=>$data];
        } else {
            $res = ['error'=>'An error occured, please refresh the page and try again.'];            
        }
        echo json_encode($res);
        exit();
    }
    public function mollie_checkout_form_scripts($prod_id){
        global $sc_currency_symbol, $sc_currency, $scp;
        ?>
        <script type="text/javascript">
            jQuery('document').ready(function($){
                // Handle form submission.
                $(document).on('studiocart/orderform/submit', '#sc-payment-form', function(event) {
                    let mollie_method = <?= json_encode(array_keys($this->mollieMethod)); ?>;
                    if ($('.pay-methods').length > 0 && !mollie_method.includes($('[name="pay-method"]:checked').val().replace("mollie_", ""))) {
                        return false;
                    }

                  var paramObj = {};
                  $.each($('#sc-payment-form').serializeArray(), function(_, kv) {
                      paramObj[kv.name] = kv.value;
                  });
                  paramObj['action'] = 'sc_mollie_request';
                  $.post(studiocart.ajax, paramObj, function(res) {
                      var response = JSON.parse(res);
                      if ('undefined' !== typeof response.error) {
                          alert(response.error);
                          $btn.removeClass('running').removeAttr("disabled");
                      } else {
                          window.location.href = response.url;
                      }
                  });
                });
            });            
        </script>
        <?php
    }
    private function sc_build_mollie_url($vars) {
        global $sc_currency;
        $sc_product_id = intval($vars['sc_product_id']);
        $itemAmount = $vars['amount'];
        $order_id = $vars['order_id'];
        if ($order_id){
            // show upsell if set
            $return_url = $vars['return_url'];
            $payment_args = array("amount" => [
                    "currency" => strtoupper($sc_currency),
                    "value" => $itemAmount
                ],
                "description" => $vars['item_name'],
                "redirectUrl" => $return_url,
                "webhookUrl"  => get_site_url(null, '/?sc-api=mollie'),
                "metadata" => [
                    "sc_order_id" => $order_id,
                ]
            );
            $payment_args["method"] = str_replace('mollie_','',$vars['pay_method']);
            if($vars['pay_method']=='mollie_ideal'){
                $payment_args["issuer"] = !empty($vars['mollie_issuer']) ? $vars['mollie_issuer'] : null;
                update_post_meta($order_id, '_sc_mollie_issuer', $vars['mollie_issuer']);
            }
            $payment = $this->mollie->payments->create($payment_args);
            update_post_meta($order_id, 'mollie_payment_id', $payment->id);
            update_post_meta($order_id, '_sc_mollie_method',$vars['pay_method']);
            return $payment->getCheckoutUrl();
        }
    }
    public function sc_mollie_method_change_request(){       
        global $sc_currency;
        if($_POST['pay-method'] == get_post_meta($_POST['sc_orderid'], '_sc_pay_method', true)){
            $res = ['error'=>'You have selected your current payment method. To change your subscription  payment method, please select another payment option.'];       
            echo json_encode($res);
            exit();
        } 
        $subscription_id = $_POST['sc_orderid'];
        $sc_product_id = get_post_meta($subscription_id, '_sc_product_id', true);
        $pay_method = $_POST['pay-method'];

        $installments = get_post_meta( $subscription_id, '_sc_sub_installments', true); 
        //$interval = get_post_meta( $post->ID, '_sc_sub_interval', true); 
       // $installments--;  
        $interval = get_post_meta( $subscription_id , '_sc_sub_interval', true );
        $interval = ($installments > 1) ? $interval.'s' : $interval; 
        $dateTime = DateTime::createFromFormat('Y-m-d', get_the_time( 'Y-m-d', $subscription_id ));
        $dateTime->add(DateInterval::createFromDateString($installments . ' ' . $interval));
        $expiresdate = $dateTime->format('Y-m-d'); 
        $date = date( 'Y-m-d');           
        if(get_post_meta( $subscription_id, '_sc_sub_installments', true) == '-1'){
            $expiresdate =  get_the_time( 'Y-m-d', $subscription_id );
            $diff = strtotime($expiresdate) - strtotime($date); 
         }else{
             $diff = strtotime($date) - strtotime($expiresdate); 
         }                        
        $number_of_days = abs(round($diff / 86400));
        if(get_post_meta( $subscription_id , '_sc_sub_interval', true ) == 'week'){
            $remainingtime = (int)(($number_of_days % 365) / 7);  
            $interval_type = 'Week';
            $subinterval = '1 weeks';
        }elseif (get_post_meta( $subscription_id , '_sc_sub_interval', true ) == 'month') {
            $remainingtime = (int)(($number_of_days % 365) / 30); 
            $interval_type = 'Month'; 
            $subinterval = '1 months'; 
        }elseif (get_post_meta( $subscription_id , '_sc_sub_interval', true ) == 'year') {
            $remainingtime = (int)($number_of_days / 365); 
            $interval_type = 'Year'; 
            $subinterval = '12 months';
        }        
        if(get_post_meta( $subscription_id, '_sc_sub_installments', true) == '-1'){
             $revenuetime = $remainingtime;  
        }else{
            $revenuetime = $installments - $remainingtime;  
        } 
        if($revenuetime == 0 || $revenuetime == '' ){
           $revenuetime = 10; 
        }
        try {
            $first_name = $_POST['first_name'].' '.$_POST['last_name'];
            $email = $_POST['email'];
            $notifyUrl = get_site_url().'/?sc-api=mollie';
            $current_user = wp_get_current_user();
            if(isset($current_user->ID) && get_user_meta($current_user->ID, 'mollie_customer_id', true) !=""){
                $customer_id = get_user_meta($current_user->ID, 'mollie_customer_id', true);
                $customer = $this->mollie->customers->get($customer_id);
            }else{
                $customer = $this->mollie->customers->create([
                    "name"    => $first_name,
                    "email"   => $email,
                    "metadata" => [
                        "isJedi" => true,
                    ],
                ]);
                update_user_meta( $current_user->ID,  'mollie_customer_id',  $customer->id );  
            }  
            $timesset = time();
            $sc_mollie_IBAN  =$_POST['_sc_mollie_IBAN'];                
            $mandate = $customer->createMandate([
                "method" => MandateMethod::DIRECTDEBIT,
                "consumerAccount" => $sc_mollie_IBAN,
                "consumerName" => $first_name,
            ]);  
            $itemAmount = get_post_meta($subscription_id, '_sc_amount', true); 
                $subscription = $customer->createSubscription([                 
                "amount"      => [
                    "currency"    => strtoupper($sc_currency),
                    "value"       => $itemAmount,
                ],
                "times"       => $revenuetime,
                "interval"    => $subinterval,
                "startDate" => date('Y-m-d'),
                "description" => sc_get_public_product_name($sc_product_id).' #'.$subscription_id.' '.$timesset,
                "webhookUrl"  => $notifyUrl,
                "method"      => null,
            ]);

            update_post_meta($subscription_id, '_sc_mollie_subscription_id', $subscription->id);
            update_post_meta($subscription_id, '_sc_mollie_customer_id', $customer->id);
            update_post_meta($subscription_id, '_sc_sub_status' , 'active' ); 
            update_post_meta($subscription_id, '_sc_status' , 'active' );
            wp_update_post( array( 'ID' =>  $subscription_id, 'post_status' => 'active' ) );
            update_post_meta($subscription_id, '_sc_pay_method', 'mollie' );
            $urlre = get_post_permalink($sc_product_id).'?sc-order='.$subscription_id;
            $res = ['url'=>$urlre];  
            echo json_encode($res); exit();  
        } catch (ApiException $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());
        } 
    }
    public function sc_mollie_request() {
        global $sc_currency;       
        if ( !wp_verify_nonce( $_POST['sc-nonce'], "sc_purchase_nonce") ) {
            echo json_encode(array(
                'error' => __("Invalid Request", "studiocartmollie"), 
            ));
            exit();
        }
        $pay_method = sanitize_text_field($_POST['pay-method']);   
        $sc_product_id = intval($_POST['sc_product_id']);
        $_sc_current_post = intval($_POST['_sc_current_post']);
        $scp = sc_setup_product($sc_product_id);
        $itemAmount = $this->calculate_cart_total();       
        $order_id = $this->save_mollie_order_to_db($scp, $itemAmount);
        $notifyUrl = get_site_url().'/?sc-api=mollie';
        if ($order_id){  
            $sc_option_id = sanitize_text_field($_POST['sc_product_option']);
            $sale = ( isset($_POST['on-sale']) && sc_is_prod_on_sale() ) ? 1 : 0;
            $plan = studiocart_plan($sc_option_id, $sale);
            $subscription_id = 0;
            if($plan->type == 'recurring'){
                $order_info = $this->order_info_from_post();
                $order_info['order_id'] = $order_id;
                $order_info['product_option'] = $_POST['sc_product_option']; 
                $subscription_id = $this->do_mollie_subscription_save($order_info);
            }
            // show upsell if set
            if ( isset($scp->upsell) ) { 
                if(!empty($scp->us_type) && $scp->us_type == 'template')
                    $return_url = get_permalink($scp->us_template);
                else if(!empty($scp->us_type) && $scp->us_type == 'page')
	                $return_url = get_permalink($scp->us_page);
                else
                    $return_url = get_permalink($_sc_current_post);
            } else {
                $return_url = get_permalink($_sc_current_post);
                $scp = sc_setup_product($sc_product_id);
                $return_url = $scp->thanks_url;
            }
            $return_url = add_query_arg(array(
                'sc-mollie-order' => $order_id,
                'sc-pid' => $sc_product_id,
                'sc-mollie' => 1,
            ),$return_url);

            $method = str_replace('mollie_','',$pay_method);
            $issuer = "";
            if($pay_method=='mollie_ideal'){
                $issuer = !empty(sanitize_text_field($_POST["issuer"])) ? sanitize_text_field($_POST["issuer"]) : null;
                update_post_meta($order_id, '_sc_mollie_issuer', $issuer);
            }
            try {
                $first_name = $_POST['first_name'].' '.$_POST['last_name'];
                $email = $_POST['email'];
                
                $is_mollie_customer = false;
                $current_user = wp_get_current_user();
                if(isset($current_user->ID) && get_user_meta($current_user->ID, 'mollie_customer_id', true) !=""){
                    $mollie_customer_id = get_user_meta($current_user->ID, 'mollie_customer_id', true);
                    if(empty($mollie_customer_id)){
                        try {
                            $is_mollie_customer = true;
                            $customer = $this->mollie->customers->get($customer_id);
                        } catch (ApiException $e) {
                            $error = 'An error occured, please refresh the page and try again.';
                        } 
                    }   
                }

                if(!$is_mollie_customer){
                    $customer = $this->mollie->customers->create([
                        "name"    => $first_name,
                        "email"   => $email,
                        "metadata" => [
                            "isJedi" => true,
                        ],
                    ]);
                }
                update_user_meta( $current_user->ID,  'mollie_customer_id',  $customer->id );
            } catch (ApiException $e) {
                $error = 'An error occured, please refresh the page and try again.';
            } 
            if($subscription_id != 0){
                $sign_up_fee = 0;
                $description = '';
                if (!empty($plan->fee) && empty($error)) :
                    $sign_up_fee = number_format((float)$plan->fee, 2, '.', '');
                    $description = 'Setup Fee and ';
                endif;
                $first_payment_amount = $sign_up_fee + $itemAmount;
                $first_payment_amount = number_format((float)$first_payment_amount, 2, '.', '');
                try{
                    $payment = $customer->createPayment([
                        "amount" => [
                            "currency" => strtoupper($sc_currency),
                            "value" => $first_payment_amount,
                        ],
                        "description"   =>  $description."First Installment #".$subscription_id,
                        "sequenceType"  =>  "first",
                        "method"        =>  $method,
                        "issuer"        =>  $issuer,
                        "webhookUrl"    =>  $notifyUrl,
                        "redirectUrl"   =>  $return_url,
                        "metadata" => [
                            "sc_order_id" => $order_id,
                            "sc_subscription_id" => $subscription_id,
                        ]
                    ]);
                }
                catch (ApiException $e) {
                    $error = htmlspecialchars($e->getMessage());
                }
                if(empty($error)):
                    $res = ['url' => $payment->getCheckoutUrl()];
                else:
                    $res = ['error'=>$error];
                endif;                
            }else{
                $payment_args = array("amount" => [
                    "currency" => strtoupper($sc_currency),
                    "value" => $itemAmount
                ],
                "description" => get_the_title($sc_product_id),
                "redirectUrl" => $return_url,
                "webhookUrl"  => $notifyUrl,
                "customerId"    => $customer->id,
                "method"        =>$method,
                "issuer"        =>$issuer,
                "metadata" => [
                    "sc_order_id" => $order_id,
                ]); 

                try{
                    $payment = $this->mollie->payments->create($payment_args);
                    update_post_meta($order_id, 'mollie_payment_id', $payment->id);
                    update_post_meta($order_id, 'mollie_customer_id', $customer->id);
                    $res = ['url'=>$payment->getCheckoutUrl()];
                } catch(ApiException $e) {
                    $res = ['error'=> htmlspecialchars($e->getMessage())];
                }
            }             
        } else {
            $res = ['error'=>'An error occured, please refresh the page and try again.'];            
        }
        echo json_encode($res);
        exit();
    }
    public function sc_mollie_process_payment() {
		global $scp;
        // add tracking/redirect for initial charge
        if ( !isset($_GET['sc-mollie']) )
			return;
        $sc_product_id = intval($_GET['sc-pid']);
        $order_id = intval($_GET['sc-mollie-order']);
        $scp = sc_setup_product($sc_product_id);
        $order_info = (array) sc_setup_order($order_id);
        // needed for main order tracking scripts
        if ( !isset($_GET['sc-oto']) ) {
            $_POST['purchase_amount'] = $order_info['amount'];
            $_POST['sc_order_id'] = $order_id;
            if(isset($order_info['bump_id'])) {
                $_POST['sc-orderbump'] = $order_info['bump_id'];
            }
        }
        if ( sc_fs()->is__premium_only() ) {
            // Show downsell
            if ( isset($_GET['sc-oto']) && $_GET['sc-oto'] == 0 && isset($scp->downsell) ) {
                $_POST['sc_downsell_nonce'] = wp_create_nonce( 'studiocart_downsell-'.$order_info['ID'] );
                return;
            }
            // Show upsell
            if ( isset($scp->upsell) && $order_info['status']=='paid') {
                $_POST['sc_order'] = $order_info;
                $_POST['sc_upsell_nonce'] = wp_create_nonce( 'studiocart_upsell-'.$order_info['ID'] );
                return;
            }
        }
        if (isset($scp->redirect_url)) {
            $redirect = esc_url(sc_personalize($scp->redirect_url, $order_info, 'urlencode'));        
            wp_redirect( $redirect );
            exit;
        }
        return;
	}
    public function save_mollie_order_to_db($scp, $_amount = false) {
        if(!$_amount) {
            $_amount = $this->calculate_cart_total();
        }
        // inital base order / order info sent via POST/ajax        
        $email = sanitize_email( $_POST['email'] );
        $customer_id ='';
        if(!empty($_POST['customer_id']))
            $customer_id = sanitize_text_field( $_POST['customer_id'] );
        $first_name = sanitize_text_field( $_POST['first_name'] ); 
        $last_name = sanitize_text_field( $_POST['last_name'] );
        $sc_product_id = intval($_POST['sc_product_id']);
        $sc_option_id = sanitize_text_field($_POST['sc_product_option']);
        $phone = (isset($_POST['phone']) && !empty($_POST['phone'])) ? sanitize_text_field( $_POST['phone'] ): null;
        $name = $first_name . ' ' . $last_name;
        $sc_accept_terms = (isset($_POST['sc_accept_terms'])) ? '' : __('accepted', "studiocartmollie");
        $pay_method = sanitize_text_field($_POST['pay-method']);
        // Find selected pay option
        $product_plan_data = $scp->pay_options;
        $sale = '';
        // Apply sale pricing?
        if ( isset($_POST['on-sale']) ){
            if ( sc_is_prod_on_sale() ) $sale = 'sale_';
        }
        foreach ( $product_plan_data as $val ) {
            if ( $sc_option_id == $val['option_id'] ) {
                $option = $val;
            }
        }
        $_item_name = $option[$sale.'option_name'];
        $_option_type = $option['product_type'];
        $plan_stripe_id = $option[$sale.'stripe_plan_id'];	
        $plan_rec_interval = $option[$sale.'interval'];	
        $plan_rec_installments = $option[$sale.'installments'];
        $ipaddress = $_SERVER['REMOTE_ADDR']; //client IP
        $curr_user_id = get_current_user_id();
        $order_info = array();
        $order_info['option_type'] = $_option_type;
        $order_info['plan']['installments'] = $plan_rec_installments;
        $order_info['amount'] = $_amount; // passed via ajax response from $this->create_payment_intent()
        $order_info['customer'] = $customer_id;
        $order_info['product_id'] = $sc_product_id;
        $order_info['firstname'] = $first_name;
        $order_info['lastname'] = $last_name;
        $order_info['name'] = $name;
        $order_info['email'] = $email;
        $order_info['phone'] = $phone;
        $order_info['item_name'] = $_item_name;
        $order_info['ip_address'] = $ipaddress;
        $order_info['user_account'] = $curr_user_id;
        $order_info['accept_terms'] = $sc_accept_terms;
        $order_info['pay_method'] = $pay_method;
        if(isset($_POST['country']) && isset($_POST['address1']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zip'])){
            $address1 = sanitize_text_field( $_POST['address1'] );
            $address2 = sanitize_text_field( $_POST['address2'] );
            $city = sanitize_text_field( $_POST['city'] );
            $state = sanitize_text_field( $_POST['state'] );
            $zip = sanitize_text_field( $_POST['zip'] );
            $country = sanitize_text_field( $_POST['country'] );
            $order_info['country'] = $country;
            $order_info['address1'] = $address1;
            $order_info['address2'] = $address2;
            $order_info['city'] = $city;
            $order_info['state'] = $state;
            $order_info['zip'] = $zip;
        }
        if ( sc_fs()->is__premium_only() ) {
            // Save coupon/bump info?
            $coupon = false;
            /*if ($subscription) {
                $order_info['coupon'] = $subscription['sub_info']['coupon'];
                if(isset($subscription['sub_info']['orderbump'])){
                    $order_info['bump_id'] = $subscription['sub_info']['orderbump']['id'];
                    $order_info['bump_amt'] = $subscription['sub_info']['orderbump']['price'];
                }
            } else {*/
                if ( isset($_POST['coupon_id']) && $_POST['coupon_id'] != '' ){
                    $coupon = sc_get_coupon( sanitize_title($_POST['coupon_id']), $sc_product_id );
                    if ( !isset($coupon['error']) ) {
                        $order_info['coupon'] = $coupon;
                    }
                }
                if ( isset($_POST['sc-orderbump']) && $scp->order_bump ) {
                    $ob_id = intval($_POST['sc-orderbump']);
                    if (intval($scp->ob_product) == $ob_id){
                        $order_info['bump_id'] = $ob_id;
                        $order_info['bump_amt'] = $scp->ob_price;
                    }
                } 
            //}
        }            
        // save order to db         
        $post_id = $this->do_mollie_order_save($order_info);
        return $post_id;
    }
    private function do_mollie_subscription_save($order){
        $post_id = wp_insert_post(array('post_title'=> time() . " " . $order['name'] , 'post_type'=>'sc_subscription', 'post_status'=>'publish'), FALSE );
        //update stripe meta
        update_post_meta($post_id, '_sc_firstname' , $order['firstname'] );
        update_post_meta($post_id, '_sc_lastname' , $order['lastname'] );
        update_post_meta($post_id, '_sc_email' , $order['email'] );
        update_post_meta($post_id, '_sc_phone' , $order['phone'] );
        update_post_meta($post_id, '_sc_country' , $order['country']??"" );
        update_post_meta($post_id, '_sc_address1' , $order['address1']??"" );
        update_post_meta($post_id, '_sc_address2' , $order['address2']??"" );
        update_post_meta($post_id, '_sc_city' , $order['city']??"" );
        update_post_meta($post_id, '_sc_state' , $order['state']??"" );
        update_post_meta($post_id, '_sc_zip' , $order['zip']??"" );
        update_post_meta($post_id, '_sc_product_id' , $order['product_id'] );
        update_post_meta($post_id, '_sc_item_name' , $order['item_name'] );
        update_post_meta($post_id, '_sc_plan_id' , $order['plan_id'] );
        update_post_meta($post_id, '_sc_amount' , $order['amount'] );
        update_post_meta($post_id, '_sc_ip_address' , $order['ip_address'] );   
        update_post_meta($post_id, '_sc_user_account' , $order['user_account'] );
        update_post_meta($post_id, '_sc_accept_terms' , $order['accept_terms'] );
        update_post_meta($post_id, '_sc_product_option', $order['product_option'] );
        update_post_meta($post_id, '_sc_pay_method', $order['pay_method'] );
        update_post_meta($post_id, '_sc_sub_amount' , $order['sub_amount'] );
        update_post_meta($post_id, '_sc_sub_item_name' ,$order['sub_item_name'] );
        update_post_meta($post_id, '_sc_sub_installments' , $order['sub_installments'] );
        update_post_meta($post_id, '_sc_sub_interval' , $order['sub_interval'] );
        update_post_meta($post_id, '_sc_sub_next_bill_date' , $order['next_bill_date'] );
        update_post_meta($post_id, '_sc_sub_end_date' , $order['sub_end_date'] );
        update_post_meta($post_id, '_sc_free_trial_days', $order['free_trial_days'] );
        update_post_meta($post_id, '_sc_sign_up_fee', $order['sign_up_fee'] );
        update_post_meta($post_id, '_sc_order_id', $order['order_id'] );
        update_post_meta($order['order_id'], '_sc_subscription_id', $post_id);
        if ( sc_fs()->is__premium_only() ) {
            if (!empty($order['coupon'])) {
                update_post_meta( $post_id ,  '_sc_coupon', $order['coupon']['code']);
            }
            if (!empty($order['bump_id'])) {
                update_post_meta($post_id, '_sc_bump_id', $order['bump_id'] );
                update_post_meta($post_id, '_sc_bump_amt', $order['bump_amt'] );
            }
        }
        return $post_id; 
    }
    private function do_mollie_order_save($order_info) {
        global $sc_currency; 
        //insert order	
        $post_id = wp_insert_post(array('post_title'=> time() . " " . $order_info['name'] , 'post_type'=>'sc_order', 'post_status'=>'publish'), FALSE );
        //update stripe meta
        update_post_meta($post_id, '_sc_firstname' , $order_info['firstname'] );
        update_post_meta($post_id, '_sc_lastname' , $order_info['lastname'] );
        update_post_meta($post_id, '_sc_email' , $order_info['email'] );
        update_post_meta($post_id, '_sc_phone' , $order_info['phone'] );
        update_post_meta($post_id, '_sc_payment_status' , 'pending' );
        update_post_meta($post_id, '_sc_status' , 'pending' );
        update_post_meta($post_id, '_sc_product_id' , $order_info['product_id'] );
        update_post_meta($post_id, '_sc_amount' , $order_info['amount'] );
        update_post_meta($post_id, '_sc_item_name' , $order_info['item_name'] );
        //update_post_meta($post_id, '_sc_plan_id' , $order_info['plan_id'] );
        update_post_meta($post_id, '_sc_ip_address' , $order_info['ip_address'] );	
        update_post_meta($post_id, '_sc_user_account' , $order_info['user_account'] );
        update_post_meta($post_id, '_sc_accept_terms' , $order_info['accept_terms'] );
        update_post_meta($post_id, '_sc_pay_method', $order_info['pay_method'] );
        update_post_meta($post_id, '_sc_country' , $order['country']??"" );
        update_post_meta($post_id, '_sc_address1' , $order['address1']??"" );
        update_post_meta($post_id, '_sc_address2' , $order['address2']??"" );
        update_post_meta($post_id, '_sc_city' , $order['city']??"" );
        update_post_meta($post_id, '_sc_state' , $order['state']??"" );
        update_post_meta($post_id, '_sc_zip' , $order['zip']??"" );
        $submitSuccess = wp_update_post( array( 'ID' =>  $post_id, 'post_status' => 'pending-payment' ) );
        //data updated
        if( $order_info['user_account'] && $current_user = get_user_by('id', $order_info['user_account']) ) {
            $user_data = array();
            if ( !$current_user->user_firstname ) $user_data['first_name'] = $order_info['firstname'];
            if ( !$current_user->user_lastname ) $user_data['last_name'] = $order_info['lastname']; 
            if ( !empty($user_data) ) {
                $user_data['ID'] = $order_info['user_account'];
                $user_id = wp_update_user( $user_data );
            }
        }
        if ( sc_fs()->is__premium_only() ) {
            if(!empty($order_info['order_type'] ) && $order_info['order_type'] == 'upsell') {
                // add upsell meta
                add_post_meta( $post_id, '_sc_us_parent', $order_info['ID'] );
                add_post_meta( $order_info['ID'], '_sc_order_child', ['id' => $post_id, 'type' => 'upsell'] );
            }
            if(!empty($order_info['order_type'] ) && $order_info['order_type'] == 'downsell') {
                // add downsell meta
                add_post_meta( $post_id, '_sc_ds_parent', $order_info['ID'] );
                add_post_meta( $order_info['ID'], '_sc_order_child', ['id' => $post_id, 'type' => 'downsell'] );
            }
            if (!empty($order_info['order_type'] ) && $order_info['coupon']) {
                sc_maybe_update_coupon_limit($order_info['coupon'], $order_info['product_id']);
                update_post_meta( $post_id , '_sc_coupon', $order_info['coupon']['code']);
            } else {
                delete_post_meta( $post_id , '_sc_coupon');                
            }
            if (isset($order_info['bump_id'])) {
                update_post_meta($post_id, '_sc_bump_id', $order_info['bump_id'] );
                update_post_meta($post_id, '_sc_bump_amt', $order_info['bump_amt'] );
            } else {
                delete_post_meta($post_id, '_sc_bump_id' );
                delete_post_meta($post_id, '_sc_bump_amt' );
            }
        }
        return $post_id;
    }
    public function sc_mollie_webhook(){
        $gateway = sanitize_text_field( get_query_var('sc-api') );
        if ( 'mollie'==$gateway && file_exists( plugin_dir_path( __FILE__ ) . 'webhook.php' ) ) {
            require(plugin_dir_path( __FILE__ ) . 'webhook.php');
        }
    }
    public function sc_mollie_checkout_page($value='')
    {
    	// show upsell if set
    	global $scp;
    	$show_confirm   = ( isset($_GET['sc-mollie-order']) && $_GET['sc-mollie-order'] > 0 ) ? true : false;
    	$orderID        = ($show_confirm) ? $_GET['sc-mollie-order'] : false;
    	$order_status   = get_post_status($orderID);
    	if($order_status=='paid'){
    		if ( isset($scp->upsell) && !empty($scp->us_type) && $scp->us_type == 'template' ) { 
	            $return_url = get_permalink($scp->us_template);
	        } else {
	            $return_url = $scp->form_action;
	        }
	        $return_url = add_query_arg(array(
			                'sc-order' => $orderID,
			                'sc-pid' => $scp->ID,
			                'sc-mollie' => 1,
			            ),$return_url);
	        ?>
	        <script type="text/javascript">
	        	window.location.href = '<?php echo $return_url; ?>';
	        </script>
	        <?php
			exit;
    	}
    }
    public function _sc_mollie_payments_error($error){
    	$show_confirm   = ( isset($_GET['sc-mollie-order']) && $_GET['sc-mollie-order'] > 0 ) ? true : false;
    	$orderID        = ($show_confirm) ? $_GET['sc-mollie-order'] : false;
    	$order_status   = get_post_status($orderID);
    	if($show_confirm){
    		switch($order_status){
	            case 'pending-payment':
	            case 'pending':
	            	$error[]=$this->molliePendingPaymentMessage;
            	break;
            	case 'uncollectible':
            		$error[]=$this->mollieFailedPaymentMessage;
            	break;
                case 'canceled':
                	$error[]=$this->mollieCanceledPaymentMessage;
                break;
                case 'past_due':
                	$error[]=$this->mollieExpirePaymentMessage;
                break;
                default:
                	//Do Nothing
                break;
            }
    	}
    	return $error;
    }
    public function sc_do_checkout_form_open($post_id) {
        global $scp;
        $show_confirm   = ( isset($_GET['sc-mollie-order']) && $_GET['sc-mollie-order'] > 0 ) ? true : false;
    	$orderID        = ($show_confirm) ? $_GET['sc-mollie-order'] : false;
    	$order_status   = get_post_status($orderID);
    	if($show_confirm){
    		switch($order_status){
	            case 'pending-payment':
	            case 'pending':
	            	$error=$this->molliePendingPaymentMessage;
            	break;
            	case 'uncollectible':
            		$error=$this->mollieFailedPaymentMessage;
            	break;
                case 'canceled':
                	$error=$this->mollieCanceledPaymentMessage;
                break;
                case 'past_due':
                	$error=$this->mollieExpirePaymentMessage;
                break;
                default:
                    $error='';
                break;
            }
    	}
        if(!empty($error)){ 
            echo '<ul class="form-errors">';
            echo '<li>'.esc_html($error, 'studiocartmollie').'</li>';
            echo '</ul>';
        }
    }
    public function mollie_transaction_id($txnid, $method, $order_id) {
        if(in_array($method, array('mollie','mollieIdeal'))) {
            if (!$id = get_post_meta($order_id, 'mollie_payment_id', true)) {
                return $id;
            } 
        }
        return $txnid;
    }
    public function mollie_sub_transaction_id($txnid, $method, $order_id) {
        if(in_array($method, array('mollie','mollieIdeal'))) {
            if (!($id = get_post_meta($order_id, 'mollie_payment_id', true))) {
                return $id;
            } 
        }
        return $txnid;
    }

    public function sc_mollie_subscription_cancel($canceled,$data){
		$mollie_error = '';
        $post_id = $data['id'];
        $subscription_id = $data['subscription_id'];
        $mollie_customer_id = get_post_meta($post_id,'_sc_mollie_customer_id',true);
        
        try{
            $customer = $this->mollie->customers->get($mollie_customer_id);
        } catch (\Mollie\Api\Exceptions\ApiException $e) {
            
            //fwrite($mollie_log, 'customer not found'); 
            $mollie_error = 'An error occured, please refresh the page and try again.';
        }
        try{
            $customer->cancelSubscription($subscription_id);
            
            $canceled = true;
        } catch (ApiException $e) {
            $mollie_error = 'An error occured, please refresh the page and try again.';
        } 
        if(!empty($mollie_error)){
            echo $mollie_error;
        }
        return $canceled;
    }

    public function sc_mollie_refund($data){
        if( !isset( $data['nonce'] ) || !wp_verify_nonce( $data['nonce'], 'sc-ajax-nonce' ) ){
            esc_html_e('Ooops, something went wrong, please try again later.', 'studiocartmollie');
            die;
        }
        if( !isset( $data['charge_id'] ) || !isset( $data['payment_intent'] )){
            esc_html_e('INVALID CHARGE ID', 'studiocartmollie');
            wp_die;
        }
        global $sc_currency;
        $post_id = intval($data['id']);
        $payment_intent = trim($data['payment_intent']);
        $amount = $data['refund_amount'];
        try{
            $payment = $this->mollie->payments->get($payment_intent);
        } catch (ApiException $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());
        } 
        
        try{
            $refund = $payment->refund([
                "amount" => [
                    "currency" => strtoupper($sc_currency),
                    "value" => number_format((float)$amount, 2, '.', '') // You must send the correct number of decimals, thus we enforce the use of strings
                ]
            ]);
            if($refund->resource == 'refund'){
                update_post_meta( $post_id, '_sc_payment_status' , "refunded" );
                wp_update_post( array( 'ID'   =>  $post_id, 'post_status'   =>  "refunded" ) );
                update_post_meta( $post_id, '_sc_status' , "refunded" );
                update_post_meta( $post_id, '_sc_refund_amount' , $amount);
                $this->sc_refund_log($post_id,$amount,$refund->id); 
                $current_user = wp_get_current_user();
                $log_entry = __( 'Payment refunded by', 'studiocartmollie' ) . ' ' . $current_user->user_login;
                sc_log_entry($post_id, $log_entry);
                if($_POST['restock'] == 'YSE'){
                    sc_maybe_update_stock( $prodID, 'increase' );
                    update_post_meta( $post_id, '_sc_refund_restock' , 'YES');
                }
                sc_trigger_integrations('refunded', $post_id);            
                esc_html_e('OK', 'studiocartmollie');
            }else{
                esc_html_e($refund->detail, 'studiocartmollie');
                wp_die;
            }  
        } catch (ApiException $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());
        } 
              
    }

    private function sc_refund_log($postID,$amount,$refundID){       
        $logs_entrie = get_post_meta( $postID, '_sc_refund_log', true);
        if(!is_array($logs_entrie)) {
            $logs_entrie = array(); 
        }
        $logs_entrie[]= array(
                            'refundID' => $refundID,
                            'date' => date('Y-m-d H:i'),
                            'amount' => $amount );

        update_post_meta( $postID, '_sc_refund_log', $logs_entrie );
    }
}
$sc_mollie = new NCS_Cart_Mollie();