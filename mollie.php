<?php
/*
Plugin Name: Studiocart Mollie Add-On
Plugin URI: https://www.studiocart.com
Description: Integrates Mollie payment method for Studiocart.
Version: 1.3.1
Author: Studiocart
Author URI: https://www.studiocart.co
License: GPL-2.0+
Text Domain: studiocartmollie
Domain Path: /languages
*/

if ( ! function_exists( 'mol_fs' ) ) {
    // Create a helper function for easy SDK access.
    function mol_fs() {
        global $mol_fs;

        if ( ! isset( $mol_fs ) ) {
            // Include Freemius SDK.
            if ( file_exists( dirname( dirname( __FILE__ ) ) . '/studiocart/freemius/start.php' ) ) {
                // Try to load SDK from parent plugin folder.
                require_once dirname( dirname( __FILE__ ) ) . '/studiocart/freemius/start.php';
            } else if ( file_exists( dirname( dirname( __FILE__ ) ) . '/studiocart-pro/freemius/start.php' ) ) {
                // Try to load SDK from premium parent plugin folder.
                require_once dirname( dirname( __FILE__ ) ) . '/studiocart-pro/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $mol_fs = fs_dynamic_init( array(
                'id'                  => '8304',
                'slug'                => 'sc-mollie',
                'premium_slug'        => 'studiocart-pro',
                'type'                => 'plugin',
                'public_key'          => 'pk_5a1387ff4278bb1a2c07eef9f7ece',
                'is_premium'          => true,
                'premium_suffix'      => 'paid',
                // If your addon is a serviceware, set this option to false.
                'has_premium_version' => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'parent'              => array(
                    'id'         => '5777',
                    'slug'       => 'studiocart',
                    'public_key' => 'pk_4d4328dbc87d3edaab1fe4158a14a',
                    'name'       => 'Studiocart',
                ),
                'menu'                => array(
                    'first-path'     => 'plugins.php',
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                'secret_key'          => 'sk_b~TR(887kVLeojVLF~o+5[$~?4Lca',
            ) );
        }

        return $mol_fs;
    }
}

function mol_fs_is_parent_active_and_loaded() {
    // Check if the parent's init SDK method exists.
    return function_exists( 'sc_fs' );
}

function mol_fs_is_parent_active() {
    $active_plugins = get_option( 'active_plugins', array() );

    if ( is_multisite() ) {
        $network_active_plugins = get_site_option( 'active_sitewide_plugins', array() );
        $active_plugins         = array_merge( $active_plugins, array_keys( $network_active_plugins ) );
    }

    foreach ( $active_plugins as $basename ) {
        if ( 0 === strpos( $basename, 'studiocart/' ) ||
             0 === strpos( $basename, 'studiocart-pro/' )
        ) {
            return true;
        }
    }

    return false;
}

function mol_fs_init() {
    if ( mol_fs_is_parent_active_and_loaded() ) {
        // Init Freemius.
        mol_fs();

        // Signal that the add-on's SDK was initiated.
        do_action( 'mol_fs_loaded' );
        require_once('class-ncs-cart-mollie-i18n.php');
        // Parent is active, add your init code here.    
        require_once( 'class-ncs-cart-mollie.php' );
        
    }
}

define( 'SC_MOLLIE_VERSION', '1.2.3' );

// If Studiocart is loaded, bootstrap the Mollie Add-On.
add_action('init', 'sc_mollie_check_parent',1);
function sc_mollie_check_parent() {
    if ( mol_fs_is_parent_active_and_loaded() ) {
        // If parent already included, init add-on.
        mol_fs_init();
    } else if ( mol_fs_is_parent_active() ) {
        // Init add-on only after the parent is loaded.
        add_action( 'sc_fs_loaded', 'mol_fs_init' );
    } else {
        // Even though the parent is not activated, execute add-on for activation / uninstall hooks.
        mol_fs_init();
    }
}