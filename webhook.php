<?php
/*
 * How to verify Mollie API Payments in a webhook.
 *
 * See: https://docs.mollie.com/guides/webhooks
 */
global $sc_currency;
$mollie_api = get_option( '_sc_mollie_api','test' );
        
$mollieApiKey = get_option('_sc_mollie_'.$mollie_api.'_api_key');
$mollie_log = fopen(plugin_dir_path( __FILE__ ) .'log_mollie.txt', 'a+');  
if(empty($_POST["id"])):
    http_response_code(400);
    exit();
endif;
try {
    /*
     * Initialize the Mollie API library with your API key.
     *
     * See: https://www.mollie.com/dashboard/developers/api-keys
     */
    
    require_once( plugin_dir_path( __FILE__ ) . '/vendor/autoload.php');

    $mollie = new \Mollie\Api\MollieApiClient();
    $mollie->setApiKey($mollieApiKey);    
    
    /*
     * Retrieve the payment's current state.
     */
    $payment = $mollie->payments->get($_POST["id"]);
    $is_subscription = false;
    if(!empty($payment->subscriptionId)){
        $is_subscription = true;
    }
    if(isset($payment->metadata) && !empty($payment->metadata->sc_subscription_id)):
        $sc_subscription_id = $payment->metadata->sc_subscription_id;
        fwrite($mollie_log, 'subscrtion_id:'.$sc_subscription_id); 
    endif;
    

    $sc_order_id = $payment->metadata->sc_order_id;
    
    fwrite($mollie_log, 'order_id:'.$sc_order_id); 

    $order_info  = array();
    /*
     * Update the order in the database.
     */
    if(!get_post($sc_order_id)):
        fwrite($mollie_log, 'order_id not found:'.$sc_order_id); 
        fclose($mollie_log);
        http_response_code(200);
        exit();
    endif;
    if(isset($sc_subscription_id) ):
        if(!get_post($sc_subscription_id)):
            fwrite($mollie_log, 'subscription_id not found:'.$sc_subscription_id); 
            fclose($mollie_log);
            http_response_code(200);
            exit();
        else:
            //fwrite($mollie_log, 'subscription creation started:'.print_r($payment,true)); 
            if(!$is_subscription):
                $customer_id = $payment->customerId;
                $mandate_id = $payment->mandateId;
                try{
                    $customer = $mollie->customers->get($customer_id);
                } catch (\Mollie\Api\Exceptions\ApiException $e) {
                    
                    //fwrite($mollie_log, 'customer not found'); 
                    $error = 'An error occured, please refresh the page and try again.';
                }
                if(empty($error)):
                    
                    if(!empty($mandate_id)):
                        try{
                            $mandate = $customer->getMandate($mandate_id);
                        } catch (\Mollie\Api\Exceptions\ApiException $e) {
                            $error = 'An error occured, please refresh the page and try again.';
                            //fwrite($mollie_log, 'mandate not found'); 
                        }
                    else:
                        $error = 'Payment Failed.';
                    endif;
                    
                endif;
                if(empty($error)):
                    $sc_sub_amount = get_post_meta($sc_subscription_id, '_sc_sub_amount', true); 
                    $sc_product_id = get_post_meta($sc_subscription_id, '_sc_product_id', true); 
                    $sc_sub_installments = get_post_meta($sc_subscription_id, '_sc_sub_installments', true); 
                    $sc_sub_interval = get_post_meta($sc_subscription_id, '_sc_sub_interval', true); 
                    $sc_free_trial_days = get_post_meta($sc_subscription_id, '_sc_free_trial_days', true); 
                    if($sc_sub_interval=='week'){
                        $sc_sub_interval = '1 weeks';
                        $interval_days = '7';
                    }elseif ($sc_sub_interval=='month') {
                        $sc_sub_interval = '1 months';
                        $interval_days = '30';
                    }elseif ($sc_sub_interval=='year') {
                        $sc_sub_interval = '12 months';
                        $interval_days = '365';
                    } 
                    if($sc_sub_installments == '-1'){
                        $sc_sub_installments = '';
                    }
                    $trial_days = 0;
                    if (!empty($sc_free_trial_days)) {
                        $trial_days = $sc_free_trial_days;
                    }
                    $next_payment_day = $trial_days+$interval_days;
                    $start_date = date('Y-m-d',strtotime(date('Y-m-d').' +'.$next_payment_day.' days'));

                    fwrite($mollie_log, 'subscription created:'.$start_date.' '.$next_payment_day); 
                    $notify_url = get_site_url().'/?sc-api=mollie';
                    try{
                        
                        $subscription = $customer->createSubscription([                 
                            "amount"      => [
                                "currency"    => strtoupper($sc_currency),
                                "value"       => $sc_sub_amount,
                            ],
                            "times"         => $sc_sub_installments,
                            "interval"      => $sc_sub_interval,
                            "startDate"     => $start_date,
                            "description"   => sc_get_public_product_name($sc_product_id).' #'.$sc_subscription_id,
                            "webhookUrl"    => $notify_url,
                            "mandateId"     => $mandate_id,
                            "metadata" => [
                                "sc_order_id" => $sc_order_id,
                                "sc_subscription_id" => $sc_subscription_id,
                            ]
                        ]); 
                        //fwrite($mollie_log, 'subscription created:'.print_r($subscription,true)); 
                        update_post_meta($sc_subscription_id, '_sc_sub_status' , $subscription->status ); 
                        update_post_meta($sc_subscription_id, '_sc_status' , $subscription->status );
                        wp_update_post( array( 'ID' =>  $sc_subscription_id, 'post_status' => $subscription->status ) );
                        update_post_meta($sc_subscription_id, '_sc_mollie_subscription_id', $subscription->id);
                        update_post_meta($sc_subscription_id, '_sc_mollie_customer_id', $customer->id);
                    } catch (\Mollie\Api\Exceptions\ApiException $e) {
                        $error = 'An error occured, please refresh the page and try again.';
                        fwrite($mollie_log, 'subscription create error:'.htmlspecialchars($e->getMessage())); 
                    }
                    
                endif;
            endif;
        endif;
    endif;
    $order_info['ID'] = $sc_order_id;
    $order_info['email'] = get_post_meta( $order_info['ID'], '_sc_email', true );
    $order_info['phone'] = get_post_meta( $order_info['ID'], '_sc_phone', true );
    $order_info['firstname'] = get_post_meta( $order_info['ID'], '_sc_firstname', true );
    $order_info['lastname'] = get_post_meta( $order_info['ID'], '_sc_lastname', true );
    $order_info['product_id'] = get_post_meta( $order_info['ID'], '_sc_product_id', true );
    $order_info['plan_id'] = get_post_meta( $order_info['ID'], '_sc_plan_id', true );
    $order_info['amount'] = get_post_meta( $order_info['ID'], '_sc_amount', true );
    $order_info['payment_status'] = $payment->status;
    $order_info['status'] = $payment->status;

    $order_type = 'main';
    if ( get_post_meta( $order_info['ID'], '_sc_us_parent', true ) ) {
        $order_type = 'upsell';
    }
    
    if ( get_post_meta( $order_info['ID'], '_sc_ds_parent', true ) ) {
        $order_type = 'downsell';
    }

    if ( sc_fs()->is__premium_only() ) {
        if ($bump_id = get_post_meta($order_info['ID'], '_sc_bump_id', true)) {

            $order_type = 'bump';
            $order_info['product_id'] = $bump_id;
            $order_info['amount'] = get_post_meta($post_id, '_sc_bump_amt', true);
            $order_info['plan_id'] = 'bump';
        }
    }
   
    foreach($order_info as $k=>$v) {
        update_post_meta( $sc_order_id , '_'.$k , $v );
    }
    
    //fwrite($mollie_log, print_r($order_info,true));
    $sub_status = 'pending';
    $post_status = 'pending';
    if ($payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks()) {
        // DO INTEGRATIONS
        $post_status = 'paid';
        $sub_status = 'active';
        $order_info['order_type'] = $order_type;
        if(isset($sc_subscription_id)):
            update_post_meta($sc_subscription_id, '_sc_sub_next_bill_date' , $start_date );
        endif;
        sc_trigger_integrations($post_status, $order_info);
        
    } elseif ($payment->isFailed()) {
        if($is_subscription):
            $sub_status = 'past_due';
        endif;
        $post_status = 'uncollectible';
        /*
         * The payment has failed.
         */
    } elseif ($payment->isExpired()) {
        if($is_subscription):
            $sub_status = 'past_due';
        endif;
        $post_status = 'past_due';
        /*
         * The payment is expired.
         */
    } elseif ($payment->isCanceled()) {
        if($is_subscription):
            $sub_status = 'canceled';
        endif;
        $post_status = 'canceled';
        /*
         * The payment has been canceled.
         */
    } elseif ($payment->hasRefunds()) {
        /*
         * The payment has been (partially) refunded.
         * The status of the payment is still "paid"
         */
    } elseif ($payment->hasChargebacks()) {
        /*
         * The payment has been (partially) charged back.
         * The status of the payment is still "paid"
         */
    }
    $order_info['post_status'] = $post_status;
    $order_info['sub_status'] = $sub_status;
    //fwrite($mollie_log, print_r($order_info,true));
    if(isset($sc_subscription_id)):
        update_post_meta($sc_subscription_id, '_sc_sub_status' , $sub_status ); 
        update_post_meta($sc_subscription_id, '_sc_status' , $sub_status );
        wp_update_post( array( 'ID' =>  $sc_subscription_id, 'post_status' => $sub_status ) );
    endif;
    wp_update_post( array( 'ID' => $sc_order_id, 'post_status'=>$post_status ), false );
    update_post_meta($sc_order_id,'_sc_payment_status',$post_status);
    update_post_meta($sc_order_id,'_sc_status',$post_status);
    sc_log_entry($order_info['ID'], __('Updating order status to ' . $order_info['payment_status'], 'ncs-cart'));
} catch (\Mollie\Api\Exceptions\ApiException $e) {
    echo "API call failed: " . htmlspecialchars($e->getMessage());
    fwrite($mollie_log, print_r($e->getMessage(),true));
}
fclose($mollie_log);
http_response_code(200);
exit();